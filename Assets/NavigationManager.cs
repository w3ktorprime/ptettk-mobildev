﻿using System;
using UnityEngine;
using TMPro;
using UnityEditor;
using UnityEngine.SceneManagement;

public class NavigationManager : MonoBehaviour
{
    [Header("Canvas elements")]
    public GameObject canvasMainMenu;
    public GameObject canvasOptions;
    public GameObject canvasCredits;

    private void Start()
    {
        // Inicializáció
        SetMainMenuActive(true);
        SetOptionsActive(false);
        SetCreditsActive(false);
    }

    public void LoadSceneByName(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    
    public void LoadSceneByIndex(int sceneId)
    {
        SceneManager.LoadScene(sceneId);
    }
    
    public void SetMainMenuActive(bool active)
    {
        canvasMainMenu.SetActive(active);
    }
    
    public void SetOptionsActive(bool active)
    {
        canvasOptions.SetActive(active);
    }
    
    public void SetCreditsActive(bool active)
    {
        canvasCredits.SetActive(active);
    }

    public void Quit()
    {
        #if UNITY_EDITOR
        Debug.Log("Quit");
        Debug.Break();
        #else 
        Application.Quit();
        #endif
    }
}
