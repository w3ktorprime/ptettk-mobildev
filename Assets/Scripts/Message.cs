﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Message : MonoBehaviour
{
    
#pragma warning disable
    [SerializeField]
    private TextMeshProUGUI senderTextTmp;
    [SerializeField]
    private TextMeshProUGUI dateTextTmp;
    [SerializeField]
    private TextMeshProUGUI messageTextTmp;
#pragma warning enable
    
    public string SenderText
    {
        // get => senderTextTmp.text;
        set => senderTextTmp.text = value;
    }

    public string DateText
    {
        // get => dateTextTmp.text;
        set => dateTextTmp.text = value;
    }
    
    public string MessageText
    {
        // get => messageTextTmp.text;
        set => messageTextTmp.text = value;
    }
}
