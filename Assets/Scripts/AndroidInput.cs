﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidInput : MonoBehaviour
{
    public Vector3 translateVector;
    public Vector3 acceleration;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        Input.gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        acceleration = Input.acceleration;
        acceleration = acceleration * speed * Time.deltaTime;
        gameObject.transform.Translate(acceleration);
    }
}
