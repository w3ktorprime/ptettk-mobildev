﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using System.Data.SQLite;

public class DatabaseManager : MonoBehaviour
{
    [SerializeField]
    private string dbName = "mydb";
    private string dbExt = ".db";
    
    // Start is called before the first frame update
    private void Start()
    {
        Debug.Log("Database path: " + Application.persistentDataPath + @"/" + dbName + dbExt);

        SQLiteConnection connection = CreateConnection();
        connection.Open();

        SQLiteCommand command = connection.CreateCommand();
        command.CommandType = CommandType.Text;
        command.CommandText = "CREATE TABLE IF NOT EXISTS 'highscores' (" +
                              " 'id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                              " 'name' TEXT NOT NULL," +
                              " 'score' INTEGER NOT NULL" +
                              ");";

        command.ExecuteNonQuery();
        connection.Close();
        
        InsertRecord("DoomSlayer", 999999);
        InsertRecord("Noob", 1);
        ReadDatabase();
    }

    private SQLiteConnection CreateConnection()
    {
        string path = Application.persistentDataPath;
        SQLiteConnection connection = new SQLiteConnection("Data Source=" + path + @"\" + dbName + dbExt + ";Version=3");
        return connection;
    }

    public void InsertRecord(string name, int score)
    {
        SQLiteConnection connection = CreateConnection();
        connection.Open();
        
        SQLiteCommand command = connection.CreateCommand();
        command.CommandType = CommandType.Text;
        command.CommandText = "INSERT INTO 'highscores' ('name', 'score') VALUES ('" + name + "', " + score + ");";

        command.ExecuteNonQuery();
        
        connection.Close();
    }

    public void ReadDatabase()
    {
        SQLiteConnection connection = CreateConnection();
        connection.Open();
        
        SQLiteCommand command = connection.CreateCommand();
        command.CommandType = CommandType.Text;
        command.CommandText = "SELECT * FROM 'highscores'";

        SQLiteDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
            Debug.Log("id: " + reader[0] + ", name: " + reader[1] + ", score: " + reader[2]);
        }
        
        connection.Close();
    }
}
