﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChatPanelManager : MonoBehaviour
{
    
#pragma warning disable
    // Managers
    [SerializeField]
    private NetworkingManager networkingManager; 
    
    // UI Elements
    [SerializeField] private GameObject chatParent;
    [SerializeField] private GameObject messageObject;
    [SerializeField] private TMP_InputField messageInputField;
    
    private Message _messageComponent;
#pragma warning enable

    public void SendMessageFromClient()
    {
        var message = messageInputField.text;
        
        // Clear input field
        messageInputField.text = "";

        // DebugPlus.LogOnScreen("Send Message From Client (MM): " + message).Duration(100).Color(Color.green);

        networkingManager.SendMessage_FromClient(message);
    }

    public void ReceiveMessage(string sender, string date, string message)
    {
        // DebugPlus.LogOnScreen("Receive Message (MM): " + sender + " - " + message).Duration(100).Color(Color.green);
        ShowInChat(sender, date, message);
    }

    private void ShowInChat(string sender, string date, string message)
    {
        // DebugPlus.LogOnScreen("Show On UI (MM): " + sender + " - " + message).Duration(100).Color(Color.green);
        var newChatMessage = Instantiate(messageObject, chatParent.transform);
        _messageComponent = newChatMessage.GetComponent<Message>();
        _messageComponent.SenderText = sender;
        _messageComponent.DateText = date;
        _messageComponent.MessageText = message;
    }
}
