﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour, IUnityAdsListener
{
    public TextMeshProUGUI rewardNumberText;
    [SerializeField] private int rewardNumber;
    
    // Start is called before the first frame update
    private void Start()
    {
        rewardNumber = 0;
        Advertisement.AddListener(this);
        Advertisement.Initialize("3557081", true);
    }
    
    public void ShowVideoAd(string placementId)
    {
        Advertisement.Show(placementId);
    }

    public void ShowBannerAd(string placementId)
    {
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(placementId);
    }

    // Events
    public void OnUnityAdsReady(string placementId)
    {
        Debug.Log("Hirdetés készen áll!");
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        Debug.Log("Hirdetés elkezdődött!");
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {
            Debug.Log("Hirdetés végignézve!");
            rewardNumber++;
            RefreshRewardedText();
        }
        else if(showResult == ShowResult.Skipped)
        {
            Debug.Log("Hirdetés átugorva!");
            rewardNumber--;
            RefreshRewardedText();
        }
        else if(showResult == ShowResult.Failed)
        {
            Debug.Log("Hirdetés hiba!");
        }
    }
    
    public void OnUnityAdsDidError(string message)
    {
        Debug.Log("Hirdetés hiba!");
    }

    // UI functions
    void RefreshRewardedText()
    {
        rewardNumberText.text = rewardNumber.ToString();
    }
}
