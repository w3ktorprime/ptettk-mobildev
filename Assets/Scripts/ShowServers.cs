﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowServers : MonoBehaviour
{
    public GameObject serverList; // Parent
    public GameObject serverPanel; // Prefab

    // Start is called before the first frame update
    void Start()
    {
        Invoke("ShowServerPanel", 3);
    }

    void ShowServerPanel()
    {
        Instantiate(serverPanel, serverList.transform);
    }
}
