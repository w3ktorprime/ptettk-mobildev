﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking;
using TMPro;

public class NetworkingManager : NetworkingManagerBehavior
{
    
#pragma warning disable
    [SerializeField] private string myName;
    private string MyName
    {
        get => myName;
        set => myName = value;
    }

    // Managers
    [SerializeField] private ChatPanelManager chatManager;
    
    // UI Elements
    [SerializeField] private TMP_InputField myNameInputField;
    [SerializeField] private TextMeshProUGUI isServerText;
    [SerializeField] private TextMeshProUGUI myIpText;
    [SerializeField] private TextMeshProUGUI myNameText;
#pragma warning enable
    
    // Run after networkObject created and initialized
    protected override void NetworkStart()
    {
        base.NetworkStart();

        isServerText.text = networkObject.IsServer ? "Server" : "Client";
        myIpText.text = networkObject.Networker.Me.IPEndPointHandle.Address.ToString();
    }

    // SERVER SIDE RPCS
    public override void SendMessage_OnServer(RpcArgs args)
    {
        var sender = args.GetNext<string>();
        var message = args.GetNext<string>();

        //DebugPlus.LogOnScreen("Send Message On Server (NM): " + sender + " - " + message).Duration(100).Color(Color.red);

        networkObject.SendRpc(RPC_RECEIVE_MESSAGE__ON_CLIENT, Receivers.All, sender, message); // Try AllBuffered
    }

    // CLIENT SIDE RPCS
    public override void ReceiveMessage_OnClient(RpcArgs args)
    {
        var sender = args.GetNext<string>();
        var message = args.GetNext<string>();
        var date = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        
        // DebugPlus.LogOnScreen("Receive Message On Client (NM): " + sender + " - " + message).Duration(100).Color(Color.green);

        chatManager.ReceiveMessage(sender, date, message);
    }

    // CLIENT SIDE METHOD
    public void SendMessage_FromClient(string message)
    {
        // DebugPlus.LogOnScreen("Send Message From Client (NM): " + message).Duration(100).Color(Color.green);
        networkObject.SendRpc(RPC_SEND_MESSAGE__ON_SERVER, Receivers.Server, MyName, message);
    }
    
    // BOTH SIDE METHOD
    public void SetMyName()
    {
        MyName = myNameInputField.text;
        myNameText.text = MyName;
    }
}
