﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchGps : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start!");
        DebugPlus.LogOnScreen("Start from debug plus!").Duration(2).Color(Color.green);
    }

    // Update is called once per frame
    void Update()
    {
        // Touch info
        if(Input.touchCount > 0)
        {
            Vector2 touchPos = Input.touches[0].position;
            DebugPlus.LogOnScreen("Touch; x: " + touchPos.x + " y: " + touchPos.y).Color(Color.black);
        }
        else
        {
            DebugPlus.LogOnScreen("There is no touch!").Color(Color.red);
        }

        // Location info
        if (Input.touchCount > 0)
        {
            LocationInfo location;
            location = Input.location.lastData;

            DebugPlus.LogOnScreen("GPS; lat: " + location.latitude + " lon: " + location.longitude).Color(Color.green);
        }
        else
        {
            DebugPlus.LogOnScreen("There is no location info!").Color(Color.red);
        }
    }
}
